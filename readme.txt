Matlab setup for a system that takes in a path or a set of paths, and outputs robot arm designs using bayesian optimization.

Requirement: Matlab 2016b (robotics toolbox for IK).
