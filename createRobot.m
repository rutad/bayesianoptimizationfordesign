function robot = createRobot(p,varargin)

displayFlag = false;
jointLim =[];

if (~isempty(varargin))
   displayFlag = varargin{1};
   if nargin>1
       jointLim = varargin{2};
   end
end

robot = robotics.RigidBodyTree;

% number of motors = number of joints
numJoints = length(p)-1; %last link should be end-effector(EE)

for i=1:numJoints
    jointName = strcat('j',num2str(i));
    limbName = strcat('l',num2str(i));
    
    % create joint and limb
    j = robotics.Joint(jointName,'revolute');
    l = robotics.RigidBody(limbName);
    if(~isempty(jointLim))
        j.PositionLimits = jointLim;
    end
    
    tvec = [p{i}(1), 0, 0];
    % set their properties using rigid body transformation
    tform = trvec2tform(tvec)*eul2tform(p{i}(2:4));
    setFixedTransform(j,tform);
    
    % add them
    l.Joint = j;
    if(~isempty(robot.Bodies))
        addBody(robot,l, robot.BodyNames{end});
    else
        addBody(robot,l,'base')
    end
end

% adding EE now
EE = robotics.RigidBody('endeffector');
tformEE = trvec2tform([p{end}(1),0,0]);%trvec2tform(p{end}(1:3))*eul2tform(p{end}(2:4)); % User defined
setFixedTransform(EE.Joint,tformEE);
addBody(robot,EE, robot.BodyNames{end});

if(displayFlag)
   showdetails(robot);
   figure, show(robot); 
end

end