%% set up a robot and path and get its cost

close all;
clear all;
clc;

%% create a robot using design params p

% p stores length and orientation of each link wrt its base/parent
% motor(link/joint). length is assumed to be along X direction
%(in the translation vector of the rigid transformation), and orientation
% is also a vector containing euler angles about [Z Y X] axis.
% p{end} is the params for the end-effector which is fixed to the last 
% motor/joint.

p{1} = [0,0,0,0];
p{2} = [1,0,0,pi/2];
p{3} = [2,0,pi/3,pi/2];
p{4}=p{2};
robot = createRobot(p,false); %last arguement is a bool to show/not show the robot made using params

%% set a path and get IK cost

% test circular path
% theta = 0:pi/20:2*pi;
% radius = 1.5;
% c_x =0; c_y = 0;
% xCircle = radius * cos(theta) + c_x;
% yCircle = radius * sin(theta) + c_y;
% path = [xCircle', yCircle', zeros(size(xCircle,2),1)];

% test random paths
pathLen = 20;
a = -2;
b = 2;
% 2D path
% path = [(b-a).*rand(pathLen,1) + a, (b-a).*rand(pathLen,1) + a, zeros(pathLen,1)];
% 3D path
path = [(b-a).*rand(pathLen,1) + a, (b-a).*rand(pathLen,1) + a, (b-a).*rand(pathLen,1) + a];

% IK
ikopts.show = true; % show arm's positions as it tracks the path
ikopts.weights = [0,0,0,1,1,1]; % set weights for EE pose during IK optimization
ikopts.maxitr = 5000; % iterations for BFGS for IK
cost = IKCost(robot, path,ikopts);

% visualize path as well
plot3(path(:,1),path(:,2),path(:,3),'g'), hold on;