function cost = IKCost(robot, path, opt)

ik = robotics.InverseKinematics('RigidBodyTree',robot);
weights = opt.weights;
if(opt.show)
    figure,show(robot),hold on;
end

% Disable random restart to ensure consistent IK solutions
if (isfield(opt, 'restart'))
    ik.SolverParameters.AllowRandomRestart = opt.restart;
end

% default num of iterations for optimization running under IK is 1500
if (isfield(opt, 'maxitr'))
    ik.SolverParameters.MaxIterations = opt.maxitr;
end

initialguess = robot.homeConfiguration;
cost = 0;

for i=1:length(path)
    if(min(size(path))<4)
        tform = trvec2tform(path(i,:));
    else
        tform = path(:,:,i);
    end
    [configSoln, solnInfo] = ik('endeffector',tform,weights,initialguess);
    initialguess = configSoln;
    
    cost = cost + solnInfo.PoseErrorNorm^2;
    % disply info 
    if(opt.show)
        show(robot,configSoln);
        new_tform = robot.getTransform(configSoln, 'endeffector', 'base');
        showEndEffectorPos(new_tform);
        fprintf('path point: %d, success:%d, error:%f, numItr:%d\n',i, solnInfo.ExitFlag, solnInfo.PoseErrorNorm, solnInfo.Iterations);
    end
end

if(opt.show)
     fprintf('net cost:%10.10f\n',cost);
end
end