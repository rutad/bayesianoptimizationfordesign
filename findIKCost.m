function cost = findIKCost(X, robot, weights, obstacles)
global target 
config = robot.homeConfiguration;
for i = 1:length(X)
    config(i).JointPosition = X(i);
end
Xee = getTransform(robot,config,'endeffector','base');
cost = poseError(target, Xee, weights) + obstacle_error(obstacles, config, robot);
end

function cost = poseError(Td, T, weights)
    %poseError
    R = T(1:3,1:3);
    Rd = Td(1:3,1:3);
    p = T(1:3,4);
    pd = Td(1:3,4);

    v = rotm2axang(Rd*R');
    err_orientation = v(4)*v(1:3)';
    err_linear = pd - p;

    errorvec = [err_orientation; err_linear ];
    cost = 0.5*errorvec'*diag(weights)*errorvec;
end

function cost = obstacle_error(obstacles, config, robot)
numbodies = robot.NumBodies;
names = robot.BodyNames;
cost = 0;
for i = 1:numbodies
    ti = getTransform(robot, config, names{i},'base');
    pos(i,:) = tform2trvec(ti);
    if(i>1)
        for j = 1:size(obstacles,1)
            x1 = pos(i-1,:);
            x2 = pos(i,:);
            x0 = obstacles(j,:);
            d = norm(cross((x0-x1), (x0-x2)))/norm(x2-x1);
            cost = cost + 100/d^2;
        end
    end
end
cost = 0;
end