%% parse data from google-brain pick and place arm dataset

clear all;
close all;
clc;

%% load all trajecories for a trial

load all_lists.mat

%% extract trajectory transforms

% allTrajs  = {approach_list, gripper_list, raise_list, present_list};
% gripper_list is a single position
% present_list is very tiny trajectory

allTrajs = {approach_list,gripper_list,raise_list};
allTrajTransforms = cell(size(allTrajs));

figure, hold on;

for j = 1:length(allTrajs)
    currentTraj = allTrajs{j};
    for i=1:6
       clear T;
       currentTraj_trialData = currentTraj{i};
       data_pos = [currentTraj_trialData(1:7:end);currentTraj_trialData(2:7:end);currentTraj_trialData(3:7:end)]'; 
       data_quat = [currentTraj_trialData(4:7:end);currentTraj_trialData(5:7:end);currentTraj_trialData(6:7:end); currentTraj_trialData(7:7:end)]'; 
       
       data_pos_sparse = data_pos(1:3:end,:) + [0.6,0,0];
       data_quat_sparse = data_quat(1:3:end,:);
   
       %data_shift_sparse = ones(
       % visualize traj
       scatter3(data_pos(1,1),data_pos(1,2),data_pos(1,3),'*'); % starting point
       plot3(data_pos(:,1),data_pos(:,2),data_pos(:,3)); % full traj
       plot3(data_pos_sparse(:,1),data_pos_sparse(:,2),data_pos_sparse(:,3)); % full sparse traj
       
       % storing..
       for k = 1:size(data_pos_sparse,1)
          T(:,:,k) = trvec2tform(data_pos_sparse(k,:))*quat2tform(data_quat_sparse(k,:)); 
       end
       currentTrajTransform{i} = T;  
    end
    allTrajTransforms{j} = currentTrajTransform;
end

%% special case of withdraw_list which is not a cell

currentTraj = withdraw_list;
for i=1:6
   clear T; 
   currentTraj_trialData = currentTraj(i,:);
   data_pos = [currentTraj_trialData(1:7:end);currentTraj_trialData(2:7:end);currentTraj_trialData(3:7:end)]'; 
   data_quat = [currentTraj_trialData(4:7:end);currentTraj_trialData(5:7:end);currentTraj_trialData(6:7:end); currentTraj_trialData(7:7:end)]'; 

   data_pos_sparse = data_pos(1:3:end,:) +[0.6,0,0];
   data_quat_sparse = data_quat(1:3:end,:);
    
   % visualize traj
   hold on;
   scatter3(data_pos(1,1),data_pos(1,2),data_pos(1,3),'*'); % starting point
   plot3(data_pos(:,1),data_pos(:,2),data_pos(:,3)); % full traj
   plot3(data_pos_sparse(:,1),data_pos_sparse(:,2),data_pos_sparse(:,3)); % full sparse traj

   % storing..
   for k = 1:size(data_pos_sparse,1)
      T(:,:,k) = trvec2tform(data_pos_sparse(k,:))*quat2tform(data_quat_sparse(k,:)); 
   end
   currentTrajTransform{i} = T;  
end
if(exist('allTrajTransforms'))
    allTrajTransforms{length(allTrajTransforms)+1} = currentTrajTransform;
else
    allTrajTransforms{1} = currentTrajTransform;
end