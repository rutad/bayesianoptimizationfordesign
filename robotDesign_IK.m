clear all;
close all;
clc;

% create rigi body link and joints
body1 = robotics.RigidBody('body1');
jnt1 = robotics.Joint('jnt1','revolute');
%jnt1.JointAxis = [0,1,0];
%jnt1.HomePosition = pi/4;
tform1 = trvec2tform([0, 0, 0]); % User defined
setFixedTransform(jnt1,tform1);
body1.Joint = jnt1;

% % create a robot as a tree structure of all limbs and joints
robot = robotics.RigidBodyTree;
addBody(robot,body1,'base')
body2 = robotics.RigidBody('body2');
jnt2 = robotics.Joint('jnt2','revolute');
% %jnt2.HomePosition = pi/6; % User defined
tform2 = trvec2tform([1, 0, 0]); % User defined
setFixedTransform(jnt2,tform2);
body2.Joint = jnt2;
addBody(robot,body2,'body1'); % Add body2 to body1

% adding end-effector now
bodyEndEffector = robotics.RigidBody('endeffector');
tform5 = trvec2tform([1, 0, 0]); % User defined
setFixedTransform(bodyEndEffector.Joint,tform5);
addBody(robot,bodyEndEffector,'body2');
 
% setting robot at a random config
% config = robot.randomConfiguration
% tform = getTransform(robot,config,'endeffector','base')

%% show robot
 figure,show(robot,robot.homeConfiguration); hold on;
% figure,show(robot,config); hold on;
%% creating a circular path
theta = 0:pi/20:2*pi;
radius = 1.5;
c_x =0; c_y = 0;
xCircle = radius * cos(theta) + c_x;
yCircle = radius * sin(theta) + c_y;

circlePath = [xCircle', yCircle', zeros(size(xCircle,2),1)];
plot3(circlePath(:,1),circlePath(:,2),circlePath(:,3),'g'), hold on;
%% ik
ik = robotics.InverseKinematics('RigidBodyTree',robot);
% Disable random restart to ensure consistent IK solutions
% ik.SolverParameters.AllowRandomRestart = false;
% ik.SolverParameters.MaxIterations = 10000;
weights = [0,0,0,1,1,1];
initialguess = robot.homeConfiguration;

%%%%% DEBUG %%%%%
% tform = trvec2tform([0,2,0]);
% showEndEffectorPos(tform);
% [configSoln, solnInfo] = ik('endeffector',tform,weights,initialguess);
% show(robot,configSoln);
% new_tform = robot.getTransform(configSoln, 'endeffector', 'base');
% showEndEffectorPos(new_tform);


for i=1:length(circlePath)
    tform = trvec2tform(circlePath(i,:));
    [configSoln, solnInfo] = ik('endeffector',tform,weights,initialguess);
    show(robot,configSoln);
    %initialguess = configSoln;
    new_tform = robot.getTransform(configSoln, 'endeffector', 'base');
    showEndEffectorPos(new_tform);
    pause(0.1);
    fprintf('itr: %d, success:%d, error:%f, numItr:%d\n',i, solnInfo.ExitFlag, solnInfo.PoseErrorNorm, solnInfo.Iterations);
end