function [reward, bestDesignParam, functionSamplesAndValues] = runBOForArm(armId, BOItr, path, samples, values)
    %% set up gpml
    addpath ../../gpml/
    startup;

    global ikopts;
    global BOopts;
    %% Calculate min cost (Cost for number of motors)
    if ~isempty(values)
        [min_cost, i] = min(values);
        x = samples(i,:);
        x_min = x;
        ind = 1;
        for i=1:4:length(x)-1
            x(i) = 0.2;
            p{ind}=x(i:i+3);
            ind = ind +1;
        end
        p{ind} = 0.2;
        
        motor_cost = robotDesignCost(p);
        if (min_cost - motor_cost < 1e-6)
            fprintf('Arm %d has converged', armId);
            reward = -motor_cost;
            bestDesignParam = x_min;
            functionSamplesAndValues.samples = samples;
            functionSamplesAndValues.values = values;
            return
        end
    end
    %% setting up robot design cost and path
    
    % cost function
    F = @(X) evaluateRobotDesignForPath(X, path, ikopts);  

    %% setting parameters for Bayesian Global Optimization
    opts = defaultopt(); % Get some default values for non problem-specific options.

    numMotors = armId; 
    opts.dims = (numMotors)*4 + 1; % Number of parameters. (4 params per link/motor + 4 params for EE).
    opts.mins = ones(1,opts.dims)*BOopts.angleBounds(1); % Minimum value for each of the parameters. Should be 1-by-opt.dims
    opts.mins(1:4:end) = BOopts.lenBounds(1);
    opts.maxes = ones(1,opts.dims)*BOopts.angleBounds(2); % Vector of maximum values for each parameter. 
    opts.maxes(1:4:end) = BOopts.lenBounds(2);
    opts.max_iters = BOItr; 
%   opt.optimize_ei = 1;

    %% Start the optimization
    fprintf('BO for robot design with motors:%d\n', armId);
    [ms,mv,T] = bayesopt_withpoints(F,samples, values, opts);   % ms - Best parameter setting found
                                   % mv - best function value for that setting L(ms)
                                   % T  - Trace of all settings tried, their function values, and constraint values.
    
    %botrace{run} = T;   
    %save('botrace_SE.mat', 'botrace')
    
    reward = -mv;
    bestDesignParam = ms;
%     ms(1:4:end)
    functionSamplesAndValues = T;

end