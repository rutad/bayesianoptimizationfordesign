%% Robot design as multi-arm bandit
clear all;
close all;
clc;

disp('Robot design as multi-arm bandit');

%% setting up robot design cost and path
% % figure 8
% t = 0:0.5:2*pi;
% x = 2*cos(t) + 2;
% y = 2*sin(2*t) / 2 +2;
% z = cos(t+0.3);
% %path = [x;y;z]';
% 
% % figure 8 with some orientation along the path
% TR = trvec2tform([0,0,0.08])*eul2tform([pi/8,0,-pi]);
% for i=1:length(x)
%    T =  trvec2tform([x(i), y(i),z(i)])*TR;
%    path(:,:,i) = T;
% end

% % SE(3) path: can grasp-release scenario:
% % Can's initial (current) pose and the desired final pose (There is a
% % translation between the two poses along the table top)
% TCanInitial = trvec2tform([0.7, 0.0, 0.55]);
% TCanFinal = trvec2tform([0.6, -0.5, 0.55]);
% 
% % Define the desired relative transform between the end-effector and the can
% % when grasping
% TGraspToCan = trvec2tform([0,0,0.08])*eul2tform([pi/8,0,-pi]);
% 
% TGrasp = TCanInitial*TGraspToCan; % The desired end-effector pose when grasping the can
% TRelease = TCanFinal*TGraspToCan; % The desired end-effector pose when releasing the can
% 
% path = exampleHelperSE3Trajectory(TGrasp, TRelease, 10); % end-effector pose waypoints

% screwing hand motion extracted from CMU mocap database, orientation
% perpendicular
% load approachGrabGoScrewDriver.mat;
% for i=1:length(grabSrewDriver)
%    pos1 = [grabSrewDriver(i,1,1),grabSrewDriver(i,1,2),grabSrewDriver(i,1,3)]/1000;
%    T1 =  trvec2tform(pos1)*eul2tform([0,0,0]);
%    path(:,:,i) = T1;
% end
% 
% load screwHandMotionPath.mat;
% for i=1:length(screwingMotion)
%    pos = [screwingMotion(i,1,1),screwingMotion(i,1,2),screwingMotion(i,1,3)]/1000;
%    T =  trvec2tform(pos)*eul2tform([0,0,-pi/2]);
%    path(:,:,length(grabSrewDriver)+i) = T;
% end

% google data set trajectories
trajName = 'googleApproachGripRaiseWithdraw052_Shifted';
load googleDataSetTrial052_transformsAll_sparse_shifted.mat
path=[];
totalCount = 0;
for j = 2:length(allTrajTransforms)
    currentTraj = allTrajTransforms{j};
    for i=1
       iTraj = currentTraj{i};
       totalCount = totalCount + size(iTraj,3);
       path(:,:,length(path)+1:(length(path)+size(iTraj,3))) = iTraj;
    end
end

assert(totalCount == size(path,3));

%% setting up global variables:
% this could be stored to keep track of what weights we used in the cost fn
% for the experiment. Format: [IKWT in netcost, DESIGNWT, LENWT];
global costWts;
costWts = [1.5, 1, 0.25];

% IK setting
global ikopts;
ikopts.show = false; % show arm's positions as it tracks the path
ikopts.weights = [1,1,1,1,1,1]; % set weights for EE pose during IK optimization
ikopts.maxitr = 1000; % iterations for BFGS for IK
ikopts.robotDesignJointLimits = [-pi, pi];
ikopts.obstacles = [];%[0.75,0.25,0.5];
ikopts.obs_radius = [];%0.25;

% BO setting bounds
global BOopts;
BOopts.lenBounds = [0.1, 0.5];
BOopts.angleBounds = [-pi, pi];

%% Multi-arm bandit optimization
% use mu to specify num motors for each arm
mu = [4,5,6,7,8]; BOItrPerArmPull = 3; initBOPulls = 3;
game = gameBODesign(mu, path, BOItrPerArmPull, initBOPulls); 

% Choice of policies to be run
policies = { policyUCB()};
% n is length of play, N is number of plays 
n = 30; N = 1;

% Time indices at which the results will be saved
tsave = round(linspace(1,n,n));
% save results in:
fname = 'results/BODesign';

% Run experiment for N times
defaultStream = RandStream.getGlobalStream;
savedState = defaultStream.State;

defaultStream.State = savedState;
tic; experiment(game, n, N, policies{1},tsave, fname); toc 

%plotResults_MAB_BO(game, n, N, policies, fname);

%% extract best design from MAB

 load([fname '_n_' num2str(n) '_N_' num2str(N) '_' class(policies{1})]);
[~,bestArmId]=max(max(cumNbPlayed));

bestParam_MAB = game.bestDesignParamForEachArm{bestArmId};

%% Continuous optimization on bestParam from MAB

[bestParam, fval] = fineTuneWithContinuousOpt(bestParam_MAB, path,mu(bestArmId));

%% visualize best design

index = 1;
for i=1:4:length(bestParam)-1
   bestP{index}=bestParam(i:i+3);
   index = index +1;
end
bestP{index} = bestParam(end);

finalRobotDesign = createRobot(bestP,true,ikopts.robotDesignJointLimits); title('optimal design for the path'); hold on;
tpath = tform2trvec(path(:,:,:));
plot3(tpath(:,1),tpath(:,2),tpath(:,3),'g');%axis(0.8*[-1 1 -1 1 -1 1]);

h = figure; %show(finalRobotDesign,finalRobotDesign.homeConfiguration); 
hold on; title('robot tarcking for the path');

ik = robotics.InverseKinematics('RigidBodyTree',finalRobotDesign);
weights = ikopts.weights; % set weights for EE pose during IK optimization
initialguess = finalRobotDesign.homeConfiguration;
for i=1:length(path)
    clf(h); %axis(0.8*[-1 1 -1 1 -1 1]);
    plot3(tpath(:,1),tpath(:,2),tpath(:,3),'g'); hold on;
    if(min(size(path))<4)
       tform = trvec2tform(path(i,:));
    else
       tform = path(:,:,i);
    end
    [configSoln, solnInfo] = ik('endeffector',tform,weights,initialguess);
    show(finalRobotDesign,configSoln);%axis(0.8*[-1 1 -1 1 -1 1]);
    initialguess = configSoln;
    new_tform = finalRobotDesign.getTransform(configSoln, 'endeffector', 'base');
    %showEndEffectorPos(new_tform);
    pause;
    fprintf('final ik itr: %d, success:%d, error:%f, numItr:%d\n',i, solnInfo.ExitFlag, solnInfo.PoseErrorNorm, solnInfo.Iterations);
end

%% saving experiment data

saveName = strcat('traj_', trajName, '_n_' ,num2str(n), '_N_', num2str(N),'.mat');

save(saveName, 'game', 'n', 'N', 'mu', 'cumReward', 'cumNbPlayed', 'tsave', ...
    'bestArmId', 'costWts','ikopts','BOopts','path');
