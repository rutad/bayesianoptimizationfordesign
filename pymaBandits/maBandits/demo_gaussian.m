disp('--- First scenario: Gaussian bandits');

% Scenario of Figure 1 in [Garivier & Cappé, COLT 2011]
% mu = [0.9, 0.8]; game = gameBernoulli(mu); fname = 'results/2a';

% Scenario of Figure 2 in [Garivier & Cappé, COLT 2011]
% mu = [0.1 0.05 0.05 0.05 0.02 0.02 0.02 0.01 0.01 0.01]; game = gameBernoulli(mu); fname = 'results/10';

% Scenario of Figure 1 in [Kaufmann, Cappé & Garivier, AISTATS 2012]
mu = [15.0, 10.0]; sig = [5, 5]; game = gameGaussian(mu, sig); fname = 'results/gaus2ab';
% Choice of policies to be run
%policies = { policyBayesUCB(), policyUCB(), policyUCBtuned()};
 policies = { policyUCB1()};
% n is length of play, N is number of plays 
n = 100; N = 2;
% Time indices at which the results will be saved
tsave = round(linspace(1,n,200));

% Run everything one policy after each other
defaultStream = RandStream.getGlobalStream;%RandStream.getDefaultStream; 
savedState = defaultStream.State;
for k = 1:length(policies)
    defaultStream.State = savedState;
    tic; experiment_original(game, n, N, policies{k}, tsave, fname); toc 
end
plotResults(game, n, N, policies, fname);