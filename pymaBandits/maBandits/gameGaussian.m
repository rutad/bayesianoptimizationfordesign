classdef gameGaussian<Game
    % Gaussian bandit game
    %
    % authors: Akshara Rai and Ruta Desai

    
    properties
        tabR % internal: array of all rewards
        N % internal: counters for rewards
        sigma
    end
    
    methods
        function self = gameGaussian(mu, sigma)
            self.mu = mu;
            self.sigma = sigma;
            %self.expectation = mu;
            self.nbActions = length(self.mu);
        end
        
        function [reward, action] = play(self, policy, n)
            policy.init(self.initRewards(n), n);
            reward = zeros(1, n);
            action = zeros(1, n);
            for t = 1:n
                action(t) = policy.decision();
                reward(t) = self.reward(action(t));
                policy.getReward(reward(t));
            end
        end
        
        function K = initRewards(self,n)
            % initiates the reward process, and returns the number of
            % actions
            K = length(self.mu);
            for a=1:K
                self.tabR(a, :) = self.mu(a) + self.sigma(a)*(randn(1,n));
            end
            %self.tabR = self.mu+self.sigma*randn(K,n);
            self.N = zeros(1,K);            
        end
        
        function r = reward(self, a)
            self.N(a) = self.N(a) + 1;
            r = self.tabR(a, self.N(a));            
        end
    end    
end
