classdef gameBODesign<Game
    %  bandit game for robot design using BO
    %
    % authors: Akshara Rai and Ruta Desai

    
    properties
        path
        BOItrForEachPull
        initPull
        samplesSoFarForEachArm
        bestDesignParamForEachArm
        xs_old
        ys_old
        flag
    end
    
    methods
        function self = gameBODesign(mu, path, BOItrForEachPull, initBOPull)
            self.mu = mu;
            self.path = path;
            self.nbActions = length(self.mu);
            self.BOItrForEachPull = BOItrForEachPull;
            self.samplesSoFarForEachArm = cell(size(mu));
            self.bestDesignParamForEachArm = cell(size(mu));
            self.xs_old = cell(size(mu));
            self.ys_old = cell(size(mu));
            self.flag = 1;
            self.initPull = initBOPull;
        end
        
        function [reward, action] = play(self, policy, n)
            policy.init(self.initRewards(n), n, self.initPull);
            reward = zeros(1, n);
            action = zeros(1, n);
            for t = 1:n
                action(t) = policy.decision();
                reward(t) = self.reward(action(t));
                policy.getReward(reward(t));
            end
        end
        
        function K = initRewards(self,n)
            % initiates the reward process, and returns the number of
            % actions
            K = length(self.mu);
            self.samplesSoFarForEachArm = cell(size(self.mu));
            self.bestDesignParamForEachArm = cell(size(self.mu));
        end
        
        function r = reward(self, a)
            if(~isempty(self.samplesSoFarForEachArm{a}))
                if(length(self.ys_old{a}) >= self.initPull*self.BOItrForEachPull)
                    if(self.flag)
                        xs = self.xs_old{a};
                        ys = self.ys_old{a};
                        self.flag = 0;
                    else
                        xs = self.samplesSoFarForEachArm{a}.samples;
                        ys = self.samplesSoFarForEachArm{a}.values;
                    end
                else
                    self.xs_old{a} = [self.xs_old{a}; self.samplesSoFarForEachArm{a}.samples];
                    self.ys_old{a} = [self.ys_old{a}; self.samplesSoFarForEachArm{a}.values];
                    xs = [];
                    ys = [];
                end
            else
                xs =[];
                ys =[];
            end
            [r, bestDesignParam, functionSamplesAndValues] = runBOForArm(self.mu(a),self.BOItrForEachPull,self.path, xs, ys);
            self.samplesSoFarForEachArm{a} = functionSamplesAndValues;
            self.bestDesignParamForEachArm{a} = bestDesignParam;
            fprintf('armId:%d, sampleSize:%d, reward:%f \n', a, length(self.samplesSoFarForEachArm{a}.values),r);
        end
    end    
end
