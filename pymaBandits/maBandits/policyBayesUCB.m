classdef policyBayesUCB < Policy
    % Bayes-UCB for Bernoulli bandits
    % (using uniform prior and 1-1/t quantile)
    % 
    % authors: Olivier Cappé, Aurélien Garivier
    % Ref.: [Kaufmann, Cappé & Garivier, AISTATS 2012]

    % $Id: policyBayesUCB.m,v 1.6 2012-06-05 13:26:38 cappe Exp $

    properties
        t % Number of the round
        lastAction % Stores the last action played
        N % Number of times each action has been chosen
        S % Cumulated reward with each action
        S2 % Cumulated squared reward with each action
        R
    end
    
    methods
        function self = policyBayesUCB()
            
        end
        
        function init(self, nbActions,horizon)
            self.t = 1;
            self.N = zeros(1, nbActions);
            self.S = zeros(1, nbActions);
            self.S2 = zeros(1, nbActions);
            self.R = cell(nbActions);
        end
        
        function [action,quantiles] = decision(self)
            if any(self.N<3)
                action = find(self.N<3, 1);
                quantiles = ones(size(self.N));
            else
                %1-1/(self.t)
                %self.S+1
                %self.N-self.S + 1
                mu_est = self.S/self.N;
                var_est = self.S2./self.N - mu_est.^2;
                quantiles = norminv(1-1/(self.t),mu_est,var_est);
                % Using beta quantile
                m = max(quantiles); I = find(quantiles == m);
                action = I(1+floor(length(I)*rand));
            end
            self.lastAction = action;
        end
        
        function getReward(self, reward)
            self.R{self.lastAction} = [self.R{self.lastAction}, reward];
            self.N(self.lastAction) = self.N(self.lastAction) + 1; 
            self.S(self.lastAction) = self.S(self.lastAction)  + reward;
            self.S2(self.lastAction) = self.S2(self.lastAction)  + reward^2;
            self.t = self.t + 1;
        end        
    end
    
end