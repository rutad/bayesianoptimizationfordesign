function [bestParam, costBestDesign] = fineTuneWithContinuousOpt(bestParamFromMAB, path, numMotors)

A = [];
b = [];
Aeq = [];
beq = [];
nonlcon = [];

global ikopts;
global BOopts;

F = @(X) evaluateRobotDesignForPath(X, path, ikopts);  

opts.dims = (numMotors)*4 + 1; % Number of parameters. (4 params per link/motor + 4 params for EE).
opts.mins = ones(1,opts.dims)*BOopts.angleBounds(1); % Minimum value for each of the parameters. Should be 1-by-opt.dims
opts.mins(1:4:end) = BOopts.lenBounds(1);
opts.maxes = ones(1,opts.dims)*BOopts.angleBounds(2); % Vector of maximum values for each parameter. 
opts.maxes(1:4:end) = BOopts.lenBounds(2);

options = optimoptions('fmincon','Display','iter','Algorithm','sqp');
[bestParam, costBestDesign] = fmincon(F, bestParamFromMAB, A, b, Aeq, beq, opts.mins, opts.maxes, nonlcon, options);

end