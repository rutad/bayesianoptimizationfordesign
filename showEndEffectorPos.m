function showEndEffectorPos( T )


s = 0.01;
[X0,Y0,Z0]=sphere;

X = s*X0 + T(1,4);
Y = s*Y0 + T(2,4);
Z = s*Z0 + T(3,4);
surf(X, Y, Z, 'facecolor','r', 'linestyle','none');

end

