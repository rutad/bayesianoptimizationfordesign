function cost = IKCost_withconstraints(robot, path, opt)

global target

weights = opt.weights;
obstacles = opt.obstacles;
ikcostfunc = @(X) findIKCost(X, robot, weights, obstacles);
configSoln = robot.homeConfiguration;
randConfig = robot.randomConfiguration;
for j = 1:length(randConfig)
    x0(j) = randConfig(j).JointPosition;
end
[x,y,z] = sphere;
r = opt.obs_radius;
cost = 0;
for i=1:1:length(path)
    if(min(size(path))<4)
        tform = trvec2tform(path(i,:));
    else
        tform = path(:,:,i);
    end
    target = tform;
    options = optimoptions('fmincon','Algorithm','sqp');%optimoptions('fmincon','Display','iter','Algorithm','sqp');
    for k = 1
        randConfig = robot.randomConfiguration;
        for j = 1:length(randConfig)
            x0(j) = randConfig(j).JointPosition;
        end
        [x_min(k,:), fval_min(k), exitflag(k)] = fmincon(ikcostfunc, x0, [], [], [], [], [], [], [], options);
    end
    [fval, idx] = min(fval_min);
    x0 = x_min(idx,:);
    for j = 1:length(x0)
        configSoln(j).JointPosition = x0(j);
    end    
    cost = cost + fval;
    % disply info 
    if(opt.show)
        show(robot,configSoln);
        hold on
        surf(r*x+obstacles(1),r*y+obstacles(2),r*z+obstacles(3))
        new_tform = robot.getTransform(configSoln, 'endeffector', 'base');
        showEndEffectorPos(new_tform);
        fprintf('path point: %d, success:%d, error:%f, numItr:%d\n',i, exitflag, sqrt(fval));
    end
end