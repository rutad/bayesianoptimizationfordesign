function cost = robotDesignCost(p)

global costWts;

%weighting constant
c_design = costWts(2);


% cost is sum of number of motors and sum of link lengths
numMotors = length(p)-1;
linkLenSum = 0;

for i =1:length(p)
    linkLenSum = linkLenSum + p{i}(1);
end

linkLenSum = linkLenSum/numMotors;
cost = c_design*(numMotors + costWts(3)*linkLenSum);

end