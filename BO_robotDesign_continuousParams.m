%% searching continuous params for a robot design with BO:
close all; clear all; clc;
addpath gpml/
startup;

%% setting up robot design cost and path
% figure 8
t = 0:0.5:2*pi;
x = cos(t);
y = sin(2*t) / 2;
z = cos(t+0.3);
path = [x;y;z]';

% IK setting
ikopts.show = false; % show arm's positions as it tracks the path
ikopts.weights = [0,0,0,1,1,1]; % set weights for EE pose during IK optimization
%ikopts.maxitr = 5000; % iterations for BFGS for IK
ikopts.robotDesignJointLimits = [-pi, pi];
ikopts.obstacles = [0.75,0.25,0.5];
ikopts.obs_radius = 0.25;

% cost function
F = @(X) evaluateRobotDesignForPath(X, path, ikopts);  

% this could be stored to keep track of what weights we used in the cost fn
% for the experiment. Format: [IKWT in netcost, DESIGNWT, LENWT];
global costWts;
costWts = [1.5, 1, 0.25];

%% setting parameters for Bayesian Global Optimization
opt = defaultopt(); % Get some default values for non problem-specific options.

numMotors = 7;
opt.dims = (numMotors)*4 + 1; % Number of parameters. (4 params per link/motor + 4 params for EE).
opt.mins = -ones(1,opt.dims)*pi; % Minimum value for each of the parameters. Should be 1-by-opt.dims
opt.mins(1:4:end) = 0.1;
opt.maxes = ones(1,opt.dims)*pi; % Vector of maximum values for each parameter. 
opt.maxes(1:4:end) = 1;
opt.max_iters = 100; 
%opt.optimize_ei = 1;

%% For starting with points - Create artificial points
xs(1,:) = 0.5*rand(1,opt.dims);
ys(1,1) = evaluateRobotDesignForPath(xs(1,:), path, ikopts);

xs(2,:) = 0.5*rand(1,opt.dims);
ys(2,1) = evaluateRobotDesignForPath(xs(2,:), path, ikopts);

%% For without initial points
% xs = [];
% ys = [];
%% Start the optimization
fprintf('Optimizing params of robot desing ...\n');
for run = 1:1%10
    tStart = tic; 
    [ms,mv,T] = bayesopt_withpoints(F, xs, ys, opt);   % ms - Best parameter setting found
                               % mv - best function value for that setting L(ms)
                               % T  - Trace of all settings tried, their function values, and constraint values.
                               
%botrace{run} = T;   
toc(tStart);
end

%save('botrace_SE.mat', 'botrace')
                              
%% Print results
fprintf('******************************************************\n');
fprintf('Best params: P = [');
for i = 1:length(ms)
    fprintf('%2.4f ',ms(i)); 
end
fprintf(']\nAssociated function value: F([P])=%2.4f\n',mv);
fprintf('******************************************************\n');