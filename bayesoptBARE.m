function [minsample,minvalue,botrace] = bayesoptBARE(F,opt)

    % Check options for minimum level of validity
    check_opts(opt);

    % Draw initial candidate grid from a Sobol sequence
	if isfield(opt,'grid')
		hyper_grid = scale_point(opt.grid,opt.mins,opt.maxes);
		opt.grid_size = size(hyper_grid,1);
	else 
    	sobol = sobolset(opt.dims);
    	hyper_grid = sobol(1:opt.grid_size,:);
		if isfield(opt,'filter_func'), % If the user wants to filter out some candidates
			hyper_grid = scale_point(opt.filter_func(unscale_point(hyper_grid,opt.mins,opt.maxes)),opt.mins,opt.maxes);
		end
	end
    incomplete = logical(ones(size(hyper_grid,1),1));

    samples = [];
    values = [];
    times = [];
    % tracking BO convergence
    minvalues = [];
    
    % hack to make BO repeatable..
    %init=[15405,19074];
    
    init = floor(rand(1,2)*opt.grid_size);
    fprintf('Running first point...\n');
    pt1 = unscale_point(hyper_grid(init(1),:),opt.mins,opt.maxes);
    % Get values for the first two samples (no point using a GP yet)
    [val1] = F(pt1);

    fprintf('Running second point...\n');
    pt2 = unscale_point(hyper_grid(init(2),:),opt.mins,opt.maxes);
    [val2] = F(pt2);
    
    incomplete(init) = false;
    samples = [samples;hyper_grid(init,:)];
    values = [values;val1;val2];
    
    % Remove first two samples from grid
    hyper_grid = hyper_grid(incomplete,:);
    incomplete = logical(ones(size(hyper_grid,1),1));

    % Main BO loop
	i_start = length(values) - 2 + 1;
    for i = i_start:opt.max_iters-2,
              
		hidx = -1;

	    [hyper_cand,hidx,aq_val] = get_next_cand(samples,values,hyper_grid,opt);
		fprintf('Iteration %d, eic = %f',i+2,aq_val);

        % Evaluate the candidate with the highest EI to get the actual function value, and add this function value and the candidate to our set.
        tic;
        value = F(hyper_cand);
        times(end+1) = toc;
        samples = [samples;scale_point(hyper_cand,opt.mins,opt.maxes)];
        values(end+1) = value;

        % Remove this candidate from the grid (I use the incomplete vector like this because I will use this vector for other purposes in the future.)
        if hidx >= 0,
        	incomplete(hidx) = false;
        	hyper_grid = hyper_grid(incomplete,:);
        	incomplete = logical(ones(size(hyper_grid,1),1));
        end
		
        minvalues = [minvalues; min(values)];
	    fprintf(', value = %f, overall min = %f\n',value,min(values));
        botrace.samples = unscale_point(samples,opt.mins,opt.maxes);
        botrace.values = values;
        botrace.times = times;
%         
%         % track BO convergence
%         plot(minvalues,'r*-');
%         title('fn min value');
%         drawnow;

        if opt.save_trace
            save(opt.trace_file,'botrace');
        end
    end
	
	% Get minvalue and minsample
    [mv,mi] = min(values);
    minvalue = mv;
    minsample = unscale_point(samples(mi,:),opt.mins,opt.maxes);


function [hyper_cand,hidx,aq_val] = get_next_cand(samples,values,hyper_grid,opt)
        % Get posterior means and variances for all points on the grid.
        [mu,sigma2,ei_hyp] = get_posterior(samples,values,hyper_grid,opt,-1);
        
        % Compute EI for all points in the grid, and find the maximum.
       	best = min(values);
        ei = compute_ei(best,mu,sigma2);

        hyps = {};
        ys = {};
        hyps{1} = ei_hyp;
        ys{1} = values;

        [mei,meidx] = max(ei);
        hyper_cand = unscale_point(hyper_grid(meidx,:),opt.mins,opt.maxes);
        hidx = meidx;
        
        aq_val = mei;

function [mu,sigma2,hyp] = get_posterior(X,y,x_hats,opt,hyp)
    meanfunc = opt.meanfunc;
    covfunc = opt.covfunc;
    if isnumeric(hyp)
        if isfield(opt,'num_mean_hypers'),
            n_mh = opt.num_mean_hypers;
        else
            n_mh = num_hypers(meanfunc{1},opt);
        end
        if isfield(opt,'num_cov_hypers'),
            n_ch = opt.num_cov_hypers;
        else
            n_ch = num_hypers(covfunc{1},opt);
        end
        hyp = [];
        hyp.mean = zeros(n_mh,1);
        hyp.cov = zeros(n_ch,1);
        hyp.lik = log(0.1);
		hyp = minimize(hyp,@gp,-100,@infExact,meanfunc,covfunc,@likGauss,X,y);
    end
    [mu,sigma2] = gp(hyp,@infExact,meanfunc,covfunc,@likGauss,X,y,x_hats);
%     fprintf('INSIDE: get_posterior() hyp.mean=%0.2f hyp.lik=%0.2f hyp.cov=\n', ...
%         hyp.mean, hyp.lik);
%     fprintf(' %0.2f', hyp.cov);
%     fprintf('\n');

function ei = compute_ei(best,mu,sigma2)
    sigmas = sqrt(sigma2);
    u = (best - mu) ./ sigmas;
    ucdf = normcdf(u);
    updf = normpdf(u);
    ei = sigmas .* (u .* ucdf + updf);

function upt = unscale_point(x,mins,maxes)
    if size(x,1) == 1,
        upt = x .* (maxes - mins) + mins;
    else
        upt = bsxfun(@plus,bsxfun(@times,x,(maxes-mins)),mins);
    end

function pt = scale_point(x,mins,maxes)
	pt = bsxfun(@rdivide,bsxfun(@minus,x,mins),maxes-mins);
    
function check_opts(opt)
    if ~isfield(opt,'dims')
        error('bayesopt:opterror',['The dims option specifying the dimensionality of ' ...
            'the optimization problem is required']);
    end
    
    if ~isfield(opt,'mins') || length(opt.mins) < opt.dims
        error('bayesopt:opterror','Must specify minimum values for each hyperparameter');
    end
       
    if ~isfield(opt,'maxes') || length(opt.maxes) < opt.dims
        error('bayesopt:opterror','Must specify maximum values for each hyperparameter');
    end 


    
function nh = num_hypers(func,opt)
    str = func();
    nm = str2num(str);
    if ~isempty(nm)
        nh = nm;
    else
        if all(str == '(D+1)')
            nh = opt.dims + 1;
        elseif all(str == '(D+2)')
            nh = opt.dims + 2;
        else
            error('bayesopt:unkhyp','Unknown number of hyperparameters asked for by one of the functions');
        end
    end


