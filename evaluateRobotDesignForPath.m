function cost = evaluateRobotDesignForPath(x, path, opts)

global costWts;

% x(1:4:end)
ind = 1;
for i=1:4:length(x)-1
   p{ind}=x(i:i+3);
   ind = ind +1;
end
p{ind} = x(end);

robot = createRobot(p,opts.show, opts.robotDesignJointLimits); 

cost = costWts(1)*IKCost(robot, path, opts) + robotDesignCost(p); %need to match units

end